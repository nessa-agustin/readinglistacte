const express = require('express')
const mongoose = require('mongoose')

const app = express();
const port = 3000;

mongoose.connect(`mongodb+srv://admin1234:admin1234@zuitt-batch197.fk6bhtr.mongodb.net/S35-C1?retryWrites=true&w=majority`,{
    useNewUrlParser: true,
    useUnifiedTopology: true
})

let db = mongoose.connection;

db.on('error', console.error.bind(console,"Connection"));
db.once('open',() => console.log('Connection to MongoDb!'));

const userSchema = new mongoose.Schema({
    firstName: String,
    LastName: String,
    username: String,
    password: String,
    email: String
})

const productSchema = new mongoose.Schema({
    name: String,
    description: String,
    price: Number
})


// Model
const User = mongoose.model('User', userSchema)
const Product = mongoose.model('Product',productSchema)

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// EndPoints

app.post('/register',(req,res) => {

    User.findOne({username: req.body.username}, (error, result) => {
        if(error){
            return res.send(error)
        }else if (result != null && result.username == req.body.username){
            return res.send(`User ${result.username} already exist`);
        }else{
            let newUser = new User({
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                username: req.body.username,
                password: req.body.password,
                email: req.body.email,
            })

            newUser.save((error, savedUser) => {
                if(error){
                    return console.error(error)
                }else{
                    return res.status(201).send({
                        message: 'New User Registered',
                        user: savedUser
                    })
                }
            })
        }
    })


})

app.post('/createProduct',(req,res) => {

    Product.findOne({description: req.body.description}, (error, result) => {
        if(error){
            return res.send(error)
        }else if (result != null && result.description == req.body.description){
            return res.send(`Product ${result.description} already exist`);
        }else{
            let newProduct = new Product({
                name: req.body.name,
                description: req.body.description,
                price: req.body.price
            })

            newProduct.save((error, savedProduct) => {
                if(error){
                    return console.error(error)
                }else{
                    return res.status(201).send({
                        message: 'New Product Created',
                        product: savedProduct
                    })
                }
            })
        }
    })


})




app.get('/users',(req,res) => {
    User.find({},(error,result) => {
        if(error){
            return res.send(error)
        }else {
            return res.status(200).json({users: result})
        }
    })
})

app.get('/products',(req,res) => {
    Product.find({},(error,result) => {
        if(error){
            return res.send(error)
        }else {
            return res.status(200).json({products: result})
        }
    })
})

app.listen(port, () => console.log(`Server is running at port:${port}`))

